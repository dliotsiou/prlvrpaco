package projectPRL

import java.io.Reader
import java.io.BufferedReader
import java.io.FileReader
import java.io.File
import java.util.Scanner
import scala.collection.mutable.HashSet

object VRP {

  //params

  //to read in data from file
  //"/Users/mimie/mimh NEW/St John's/year3/Part II Project/Actual/Datasets/Augerat et al instances - Sets A, B and P/Set A/A-VRP/A-n45-k6.vrp"
  var datasetsPath: String = ""
  var augeral: String = "/Augerat et al instances - Sets A, B and P"
  var augeral_A: String = datasetsPath + augeral + "/Set A/A-VRP"
  var augeral_B: String = datasetsPath + augeral + "/Set B/B-VRP"
  var augeral_P: String = datasetsPath + augeral + "/Set P/P-VRP"

  var christofides: String = "/Christofides and Eilon/CE-VRP"

  //  var datafile: String = "n53-k7" //should be arg in main
  //
  //  var filename: File = new File(augeral_A + "/A-" + datafile + ".vrp")
  //  //  var reader: Reader = new FileReader(filename)
  //  var buffRdr = new BufferedReader(new FileReader(filename): Reader) //--> put in main?

  var name, comment, problemType, edgeWeightType = ""
  var dimension, capacity = 0

  type t = (Int, Int)
  var nodeCoords = new Array[t](dimension)
  //  var distList: List[Double] = Nil
  var distListArray = Array[Double](dimension)
  //  var distMatrix: List[List[Double]] = Nil
  var distMatrixArray = Array.ofDim[Double](dimension, dimension)
  var depot: Int = 0
  var altDepot = new HashSet[Int]
  var nodeDemandArray = new Array[Int](dimension)

  var nodes = new HashSet[Int]
  var nodesNorm = new HashSet[Int]
  var nodesExcDepots = new HashSet[Int]
  var depotsNorm = new HashSet[Int]

  //to read in best known route distances
  //".../Datasets/Augerat et al instances - Sets A, B and P/Set A/A-VRP-sol/opt-A-n45-k6"

  //  var filenameSol: File = new File(augeral_A_opt + "/opt-A-" + datafile)
  ////  var readerSol: Reader = new FileReader(filenameSol)
  //  var buffRdrSol = new BufferedReader(new FileReader(filenameSol):Reader)

  var solCost = 0.0
  def readFile(buffRdr: BufferedReader) = {

    var newline = buffRdr.readLine()
    while (newline != null && newline != "EOF") {

      //NOTE: IDEALLY DON'T RECHECK SOMETHNG ONCE IT'S BEEN SET -- FLAGS!
      //ALSO, GENERALISE: filepath, other possible name-value pairs

      if (newline.startsWith("NAME : ")) {
        name = newline.substring("NAME : ".length)
        System.out.println(name)
      }
      if (newline.startsWith("COMMENT : ")) {
        comment = newline.substring("COMMENT : ".length)
        System.out.println(comment)
        //here: read optimal value from comment variable.

        var cmtScanner = new Scanner(comment)

        assert(comment.containsSlice("value: "))
        var index = comment.indexOfSlice("value: ")
        var cmt2 = comment.drop(index + "value: ".length)
        var cmt3 = cmt2.dropRight(1)

        solCost = cmt3.toInt.toDouble
        System.err.println("solCost= " + solCost)
        System.err.println("")

      }
      if (newline.startsWith("TYPE : ")) {
        problemType = newline.substring("TYPE : ".length)
        System.out.println(problemType)
      }
      if (newline.startsWith("DIMENSION : ")) {
        dimension = newline.substring("DIMENSION : ".length).toInt
        System.out.println(dimension)
      }
      if (newline.startsWith("EDGE_WEIGHT_TYPE : ")) {
        edgeWeightType = newline.substring("EDGE_WEIGHT_TYPE : ".length)
        System.out.println(edgeWeightType)
      }
      if (newline.startsWith("CAPACITY : ")) {
        capacity = newline.substring("CAPACITY : ".length).toInt
        System.out.println(capacity)
      }

      if (newline.startsWith("NODE_COORD_SECTION")) {

        nodeCoords = new Array[t](dimension) //KEEP HERE b/c of dimension.

        System.out.println("for -coords")
        for (i <- 0 to (dimension - 1) by +1) {
          newline = buffRdr.readLine();
          var linescanner = new Scanner(newline)
          val index = linescanner.nextInt()
          assert(i == index - 1) //
          nodeCoords(i) = (linescanner.nextInt(), linescanner.nextInt())
          System.out.println(index + "'s coords : " + nodeCoords(i))
        }
      }

      if (newline.startsWith("DEMAND_SECTION")) {

        var nodeDemand = new Array[Int](dimension)

        System.out.println("for -demand")
        for (i <- 0 to (dimension - 1) by +1) {
          newline = buffRdr.readLine();
          var linescanner = new Scanner(newline)
          val index = linescanner.nextInt()
          assert(i == index - 1) //
          nodeDemand(i) = (linescanner.nextInt())
          System.out.println(index + "'s demand : " + nodeDemand(i))
          nodeDemandArray = nodeDemand
        }
      }
      if (newline.startsWith("DEPOT_SECTION")) {
        newline = buffRdr.readLine();
        while (!newline.startsWith(" -1")) {
          var linescanner = new Scanner(newline)
          altDepot.add(linescanner.nextInt()) // :: altDepot
          System.out.println("altDepot: " + altDepot)
          newline = buffRdr.readLine();
        }
      }
      newline = buffRdr.readLine()
    }

    for (i <- 0 to (dimension - 1)) {
      nodes.add(i + 1) //nodes start at 1, there is no node 0
    }

    //    nodes = nodes.reverse

    for (n <- 0 to nodes.size - 1) {
      //    nodes.foreach { n =>
      //       var m: Int = n-1
      var m: Int = n //was n-1 //0 to 31, instead of 1 to 32
      nodesNorm.add(m)
    }
    //    nodesNorm = nodesNorm.reverse //ok

    val depots = altDepot
    //NEW

    var depotsNormList: List[Int] = Nil
    val depotsArray = depots.toArray

    for (n <- 0 to depotsArray.size - 1) {
      //    depots.foreach { n =>
      var m: Int = depotsArray(n) - 1
      depotsNorm.add(m)
      depotsNormList = m :: depotsNormList
    }
    depotsNormList.reverse
    depot = depotsNormList.head
    //    depotsNorm = depotsNorm.reverse

    //    nodesExcDepots = new java.util.HashSet(nodesNorm) //take copy of nodesNorm
    nodesExcDepots.++=(nodesNorm) //~ take copy of nodesNorm ?
    nodesExcDepots.retain(n => !depotsNorm.contains(n))
    //    var nodesExcDepotsList = nodesNorm.asInstanceOf[scala.collection.mutable.HashSet[Int]].toList

  }

  def calcdistances() = { //use distance formula, save results in a Matrix

    if (edgeWeightType == "EUC_2D" | edgeWeightType == "EUC_2D ") {

      assert(nodeCoords != null)

      distMatrixArray = Array.ofDim[Double](dimension, dimension)

      for (i <- 0 to nodeCoords.length - 1) {
        val e1 = nodeCoords(i)

        var distListArray = Array.ofDim[Double](dimension) //move above inner foreach?
        //      nodeCoords.foreach { e1 =>
        for (j <- 0 to nodeCoords.length - 1) {
          val e2 = nodeCoords(j)
          //        nodeCoords.foreach { e2 =>
          val dist = { math.sqrt((e1._1 - e2._1) * (e1._1 - e2._1) + (e1._2 - e2._2) * (e1._2 - e2._2)) }
          distListArray(j) = dist
          //System.out.println(distList) 
        }
        distMatrixArray(i) = distListArray
      }
      System.out.println("")
    }
  }

  def readSol(buffRdrSol: BufferedReader) = { //Don't use this, take optimal solution from input file above.
    var newline = buffRdrSol.readLine()
    while (newline != null && newline != "EOF") {
      System.out.println(newline)
      if (newline.startsWith("cost ") | newline.startsWith("Cost ")) {
        solCost = newline.substring("cost ".length).toDouble
      }
      newline = buffRdrSol.readLine()
    }
  }
  var aco: ACO = null
  var iterations: Int = 0
  var flag2opt: Int = 0 //0 means false, no optim
  var noThreads: Int = 0
  var noAnts: Int = 25

  def main(args: Array[String]) = {

    datasetsPath = args(0)
    var fileGroup: String = args(1)
    var datafile: String = args(2)

    System.err.println("datasetsPath= " + datasetsPath)
    System.err.println("datafile= " + datafile)
    iterations = args(3).toInt
    flag2opt = args(4).toInt //1 if want optim
    noThreads = args(5).toInt //ACTUALLY, NUMBER OF GROUPS!!!
    noAnts = args(6).toInt

    var prefix: String = ""

    if (fileGroup == "aug-a") {
      fileGroup = augeral_A
      prefix = "A-"
    } else if (fileGroup == "aug-b") {
      fileGroup = augeral_B
      prefix = "B-"
    } else if (fileGroup == "aug-p") {
      fileGroup = augeral_P
      prefix = "P-"
    } else if (fileGroup == "ce") {
      fileGroup = christofides
      prefix = "E-"
    }

    //	    var filename: File = new File(datasetsPath + augeral_A + "/A-" + datafile + ".vrp")
    var filename: File = new File(datasetsPath + fileGroup + "/" + prefix + datafile + ".vrp")

    var buffRdr = new BufferedReader(new FileReader(filename): Reader)
    readFile(buffRdr)

    calcdistances() //calculate distances

    //     var filenameSol: File = new File(datasetsPath + augeral_A_opt + "/opt-A-" + datafile)
    //    var filenameSol: File = new File(datasetsPath + fileGroup + "-sol" + "/opt-" + prefix + datafile)
    //    var buffRdrSol = new BufferedReader(new FileReader(filenameSol): Reader)
    //    readSol(buffRdrSol)

    aco = new ACO(noAnts) //moved above
    aco.ACOalg()
  }
}