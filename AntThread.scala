package projectPRL

import projectPRL._

class AntThread extends Thread {

  var k: Ant = null
  var hetaMatrix: List[List[Double]] = Nil
  var pheroMatrix = Array.ofDim[Double](dimension, dimension)

  var dimension: Int = VRP.aco.dimension
  var alpha: Double = VRP.aco.alpha
  var t0: Double = VRP.aco.t0
  
  //  var depotsNorm: List[Int] = VRP.aco.depotsNorm
  val distMatrixArray = VRP.distMatrixArray
  var depot = VRP.depot

  def setParams(k: Ant, hetaMatrix: List[List[Double]], pheroMatrix: Array[Array[Double]] /*, dimension: Int, alpha: Double, t0: Double, nodesExcDepots: List[Int], depotsNorm: List[Int], distMatrix: List[List[Double]]*/ ) = {
    this.k = k
    this.pheroMatrix = pheroMatrix
    this.hetaMatrix = hetaMatrix

    //    this.dimension=dimension
    //    this.alpha = alpha
    //    this.t0=t0
    //    this.nodesExcDepots=nodesExcDepots
    //    this.depotsNorm = depotsNorm
    //    this.distMatrix = distMatrix
  }

  //run must have NO ARGUMENTS, what to do, esp. with k?
  override def run() = {

    //all the stuff in the ants.foraech loop:
    System.out.println("")
    System.out.println("Ant: " + k)

    k.nextDestination = depot // node 1 is the depot, nodeNorm 0 is the depot
    k.setLocation(depot)
        k.visitedNodesSet.clear()
    k.visitedNodesVector.clear

    k.eligibleNodesSet.clear()
    k.eligibleNodesSet.++=(VRP.nodesExcDepots)
    k.eligibleNodesArray= k.eligibleNodesSet.toArray[Int]
    k.routeMatrix = Nil
    var routeIndex = -1

    k.solutionDistance = 0

    while (k.visitedNodesSet.size < dimension & !k.eligibleNodesSet.isEmpty) { //until one full solution is completed

      //capacity-related stuff:
      k.location = depot
      k.visitedNodesSet.add(k.location)
      k.visitedNodesVector.add(k.location) //TODO: check added at the right place!

      k.eligibleNodesSet.retain(n => !k.visitedNodesSet.contains(n)) //TODO: Probably redundant.
      k.eligibleNodesArray = k.eligibleNodesArray.filterNot(n => k.visitedNodesSet.contains(n))
      k.load = 0
      routeIndex += 1

      var routeNodes: List[Int] = Nil

      while (k.load <= k.capacity & k.visitedNodesSet.size < dimension & !k.eligibleNodesSet.isEmpty) { //i.e. for each route
        //System.err.println("k.visitedNodes.distinct.length: "+k.visitedNodes.distinct.length)
        k.nextDestination = VRP.aco.chooseNext(k, pheroMatrix, hetaMatrix)
        k.load += VRP.nodeDemandArray(k.nextDestination) //No off-by-1.

        if (k.load <= k.capacity) { //if node doesn't overflow the limit, go there
          k.setLocation(k.nextDestination) //move to next customer - set location to nextDestination chosen in previous step
          k.visitedNodesSet.add(k.location)
          k.visitedNodesVector.add(k.location) //TODO: check added at the right place!
          routeNodes = k.location :: routeNodes
          k.eligibleNodesSet.remove(k.location) //NEW
          k.eligibleNodesArray = k.eligibleNodesArray.filterNot(n => n == k.location)
        }
      } //end of inner while (for each route)

      routeNodes = routeNodes.reverse
      
      // HERE: call 2-opt, if enabled:
      if (VRP.flag2opt == 1) {
        val oldRouteNodes = routeNodes
        routeNodes = VRP.aco.twoOpt(routeNodes)
      }

      var routeNodesArray = routeNodes.toArray
      
      k.routeMatrix = routeNodes :: k.routeMatrix

      //Pretty Print stuff:

      //denorm means denormalised:

      System.out.println("")
      System.out.println("")
      System.out.println("route # " + routeIndex + ": ")
      //          System.out.println("routeNodes: " + routeNodes)
      //denorm:
      System.out.print("routeNodes: ")
      routeNodes.foreach { n =>
        System.out.print(n + 1 + " ")
      }
      System.out.println("")

      var cumulativeDist: Double = 0
      var cumulativeDemand: Int = 0
      var dist: Double = 0
      for (i <- 0 to routeNodesArray.length - 1) { //CHECK this "length-2" everywhere, does it calculate ALL distances, even for last node in list?
        if (i == 0) {
          var distToDepot = distMatrixArray(depot)(routeNodesArray(i))
          var pheroToDepot = pheroMatrix(depot)(routeNodesArray(i))
          //denorm:
          System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + (routeNodesArray(i) + 1) + "(" + VRP.nodeDemandArray(routeNodesArray(i)) + ")")
          dist += distToDepot
        } else if (i == (routeNodesArray.length - 1)) {
          dist = distMatrixArray(routeNodesArray(i - 1))(routeNodesArray(i))
          var phero = pheroMatrix(routeNodesArray(i - 1))(routeNodesArray(i))
          var distToDepot: Double = distMatrixArray(routeNodesArray(i))(depot)
          var pheroToDepot = pheroMatrix(routeNodesArray(i))(depot)
          //denorm:
          System.out.print(" - " + dist + "d, " + phero + "p - " + (routeNodesArray(i) + 1) + "(" + VRP.nodeDemandArray(routeNodesArray(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")

          dist += distToDepot
        } else {
          dist = distMatrixArray(routeNodesArray(i - 1))(routeNodesArray(i))
          var phero = pheroMatrix(routeNodesArray(i - 1))(routeNodesArray(i))
          //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")

          //denorm:
          System.out.print(" - " + dist + "d, " + phero + "p - " + (routeNodesArray(i) + 1) + "(" + VRP.nodeDemandArray(routeNodesArray(i)) + ")")
        }

        cumulativeDist += dist
        cumulativeDemand += VRP.nodeDemandArray(routeNodesArray(i))
      }
      assert(cumulativeDemand <= VRP.capacity)

      k.solutionDistance += cumulativeDist

      System.out.println("")
      System.out.println("cumulative Distance (for each route): " + cumulativeDist)
      System.out.println("cumulative Demand: " + cumulativeDemand)

      //denorm:
      System.out.println("")
      System.out.print("k.visitedNodes: ")
      for (n <- 0 to k.visitedNodesVector.size - 1) {
        //        k.visitedNodes.foreach { n =>
        System.out.print((k.visitedNodesVector.get(n) + 1) + " ")
      }
      System.out.println("")

      var visitedNodesList: List[Int] = Nil //(k.visitedNodesVector.toArray).toList
      for (i <- 0 to k.visitedNodesVector.size - 1) {
        visitedNodesList = k.visitedNodesVector.get(i) :: visitedNodesList
      }
      visitedNodesList.reverse
      val sorted = visitedNodesList.sortWith((e1, e2) => (e1 < e2))

      System.out.print("sorted k.visitedNodesVector: ")
      sorted.foreach { n =>
        System.out.print((n + 1) + " ")
      }
      System.out.println("")

    } //end of  outer while

    //go back to the depot once eligibleNodes = Nil:
    k.nextDestination = depot
    //    k.visitedNodes = k.nextDestination :: k.visitedNodes
    k.visitedNodesSet.add(k.nextDestination)
    k.visitedNodesVector.add(k.nextDestination) //TODO: check added at the right place!

    //    k.visitedNodes = k.visitedNodes.reverse

    k.routeMatrix = k.routeMatrix.reverse

    System.out.println("solutionDistance: " + k.solutionDistance)

    //update phero on all arcs:
    for (i <- 0 to (k.visitedNodesVector.size - 2)) { //NEW  took away a -1
      val newPhero: Double = (1 - alpha) * { pheroMatrix(k.visitedNodesVector.get(i))(k.visitedNodesVector.get(i + 1)) } + alpha * t0 //was =t0 again !
      //          pheroMatrix = pheroMatrix.updated(k.visitedNodes(i), pheroMatrix(k.visitedNodes(i)).updated(k.visitedNodes(i + 1), newPhero))
      pheroMatrix(k.visitedNodesVector.get(i)).update(k.visitedNodesVector.get(i + 1), newPhero)
    }
  }
}