package projectPRL

import projectPRL._
import scala.util.Random
import scala.collection.mutable.MutableList
import java.util.Calendar
import scala.collection.mutable.HashSet

class ACO(var m: Int) {

  val dimension = VRP.dimension //32

  val nodes = VRP.nodes
  val nodesNorm = VRP.nodesNorm
  val nodesExcDepots = VRP.nodesExcDepots
  val depotsNorm = VRP.depotsNorm
  val depot = VRP.depot

  var groups: Int = VRP.noThreads

  var ants: List[Ant] = Nil

  for (i <- 0 to m - 1) { //m= # of ants, here 25 ants
    var i = new Ant
    i.capacity = VRP.capacity
    ants = i :: ants
  }
  ants = ants.reverse

  if (groups > ants.length) groups = ants.length

  var groupSize = math.floor(ants.length / groups).toInt
  val remainder = (ants.length % groups)

  val distMatrixArray = VRP.distMatrixArray
  var iterations = VRP.iterations

  //params for eqns (1) to (4):
  //(1)

  //Skip the closest-neighbour heuristic (not sure whether it should respect capacities),
  //set L2 known optimal:
  val L2 = VRP.solCost

  System.err.println("L2 = " + L2)
  System.err.println("")

  val t0: Double = 1.0 / (L2 * (dimension - 1)) //dimension-1
  System.err.println("t0= " + t0)

  var randomGenQ = new Random()
  var q0: Double = 0.9 //means eqn (2) is called rarely, since most numbers ~U[0,1] are <0.9
  var beta: Double = 2.3
  //val t0: Double = 1.0 / VRP.solCost // 1/(best known distance) = 1/784 = 0.0012755102040816326 

  //(2)
  //  var randomGenS2 = new Random
  //  var s2 = randomGenS2.nextDouble //the pseudo-random RV ~ U[0,1]

  def sigmaSum(array: Array[Double], l: Int, u: Int, sum: Double, indexArray: Array[Int]): Double = { //big-sigma sum with lower and upper bounds

    if (array.sum == 0.0) {
      //		      System.err.println("probMatrix.sum = 0.0 ")
    }
    //		    System.err.println("probMatrix.sum = " + array.sum)
    var localSum = sum
    if (l == u) {
      try {
        localSum += array(indexArray(u)) //ArrayIndexOutOfBounds
      } catch {
        case indexoutofbounds: ArrayIndexOutOfBoundsException =>
          System.err.println("l= " + l + ", u= " + u + ", indexArray.length = " + indexArray.length)
      }
      return localSum
    } else {
      if (l < u) {
        localSum = {
          sigmaSum(array, l + 1, u, localSum + array(indexArray(l)), indexArray)
        }
      }
      return localSum
    }
  }
  //(3)
  val alpha: Double = 0.1

  //  var iterSolutionList: List[Double] = Nil
  var iterSolutionSet = new HashSet[Double]

  def chooseNext(k: Ant, pheroMatrix: Array[Array[Double]], hetaMatrix: List[List[Double]]): Int = {

    k.eligibleNodesSet.retain(n => !k.visitedNodesSet.contains(n)) //Change if candidate list enabled. //there's a -1 here?!?
    //    k.eligibleNodesVector.retainAll(k.eligibleNodesSet)
    k.eligibleNodesArray = k.eligibleNodesSet.toArray[Int] //k.eligibleNodesArray.filterNot(n => k.visitedNodesSet.contains(n))

    assert(k.visitedNodesSet.size <= dimension)
    //chooseNext customer, BUILD A JOB SEQUENCE, I.E. FIND ALL CUSTOMERS
    var q = randomGenQ.nextDouble()
    //            System.err.println("start of while, \nq =" + q)
    if (q <= q0) {
      var localnodesExcDepots = new HashSet[Int]
      localnodesExcDepots.++=(nodesExcDepots) // ~ made copy
      var us = localnodesExcDepots
      us.retain(n => !k.visitedNodesSet.contains(n))
      var criterion1List: List[(Double, Int)] = Nil
      var criterion1bList: List[Double] = Nil

      //eqn (1):
      if (us != Nil) { //if some nodes!=depot still left to visit
        us.foreach { u =>
          var criterion1 = { pheroMatrix(k.location)(u) } * Math.pow({ hetaMatrix(k.location)(u) }, beta)
          criterion1List = (criterion1, u) :: criterion1List
          criterion1bList = criterion1 :: criterion1bList
        }
        criterion1List = criterion1List.reverse
        var filteredCriterion1List = criterion1List.filter(c1 => c1._1 != Double.PositiveInfinity && c1._1 != Double.NegativeInfinity)
        var filteredCriterion1bList = criterion1bList.filter(c => c != Double.PositiveInfinity && c != Double.NegativeInfinity)
        var tupleMax = filteredCriterion1List.find(c1 => (c1._1) == filteredCriterion1bList.max)
        //                System.err.println("tupleMax from eqn(1): " + tupleMax) //None. b/c infinity is the max.
        if (tupleMax != None) k.nextDestination = (tupleMax.get)._2
        //                System.err.println("k.nextDestination from eqn(1): " + k.nextDestination)
      } else { //if visited all nodes there are
        k.nextDestination = depot
      }
    } else {
      //eqn (2):

      k.probMatrix = Array.ofDim[Double](dimension, dimension)

      for (i <- 0 to (dimension - 1)) {
        var probList = Array.ofDim[Double](dimension)
        for (j <- 0 to (dimension - 1)) {
          var us = k.eligibleNodesArray
          if (us.contains(j)) { //was: if (!k.visitedNodes.contains(j) && !depotsNorm.contains(j))
            var denomElements = new scala.collection.mutable.HashSet[Double]
            us.foreach { u =>
              var denomElement = { pheroMatrix(k.location)(u) } * Math.pow({ hetaMatrix(k.location)(u) }, beta)
              denomElements.add(denomElement)
            }
            val prob = { { pheroMatrix(k.location)(j) } * Math.pow({ hetaMatrix(k.location)(j) }, beta) } / denomElements.sum
            probList(j) = prob
          } else probList(j) = 0.0
        }
        //        probList = probList.reverse
        k.probMatrix(i) = probList
      }
      //     probMatrix = probMatrix.reverse // all 0s.
      //probMatrix has been constructed, needs to be recalculated for each chooseNext, because pheroMatrix and visited nodes change.
      // also pheroMatrix needs to be recalculated for each chooseNext (--CHECK)

      def invTransform(n: Int): Int = {

        //        if (n < probMatrix(k.location).length) {

        if (sigmaSum(k.probMatrix(k.location), 0, (n - 1), 0.0, k.eligibleNodesArray) <= k.s2
          & k.s2 < sigmaSum(k.probMatrix(k.location), 0, (n), 0.0, k.eligibleNodesArray)) { //NOTE:k.eligibleNodesArray.lenght must be = n+1 //was n
          //                    System.err.println("sigmaSum(probMatrix(k.location), 0, (n - 1): " + sigmaSum(probMatrix(k.location), 0, (n - 1)))
          //                    System.err.println("s2: " + s2)
          //                    System.err.println("sigmaSum(probMatrix(k.location), 0, n): " + sigmaSum(probMatrix(k.location), 0, n))
          return k.eligibleNodesArray(n) //? //eligibleNodes(n) 
        } else {
          //                    System.err.println("sigmaSum(probMatrix(k.location), 0, (n - 1): " + sigmaSum(probMatrix(k.location), 0, (n - 1)))
          //                    System.err.println("s2: " + s2)
          //                    System.err.println("sigmaSum(probMatrix(k.location), 0, n): " + sigmaSum(probMatrix(k.location), 0, n))
          return invTransform(n + 1)
        }
      }

      k.s2 = k.randomGenS2.nextDouble
      //              System.err.println("s2: " + s2)
      //              System.err.println("eligible nodes: " + eligibleNodes)
      if (!k.eligibleNodesSet.isEmpty) {
        if (k.s2 < k.probMatrix(k.location)(k.eligibleNodesArray(0))) {
          k.nextDestination = k.eligibleNodesArray(0)
        } else {
          k.nextDestination = invTransform(1) //was 1
        }
      } else {
        return depot
      }
    } //end of eqn (2)

    return k.nextDestination
  } //end of chooseNext

  def ACOalg() = {

    //var calendar: Calendar = Calendar.getInstance()
    var timeAtStart = System.currentTimeMillis() / 1000 //seconds

    //initialize all trails to t0:
    //    var pheroList = new MutableList[Double] //one row of tau iu, for all u not in Mk //moved inside outer for below
    var pheroMatrix = Array.ofDim[Double](dimension, dimension)

    for (i <- 0 to (dimension - 1)) {
      var phero1DArray = Array.ofDim[Double](dimension)
      for (j <- 0 to (dimension - 1)) {
        val phero = t0
        phero1DArray(j) = phero //don't need to reverse
      }
      pheroMatrix(i) = phero1DArray //don't need to reverse
    }

    //initialize heta i-u = 1/(distance i-u)
    var hetaList: List[Double] = Nil
    var hetaMatrix: List[List[Double]] = Nil

    for (loc <- 0 to distMatrixArray.length - 1) {
      hetaList = Nil //!
      for (n <- 0 to distMatrixArray(loc).length - 1) {
        var heta: Double = 0.0
        if (distMatrixArray(loc)(n) == 0.0 & loc != n) { //if diff. nodes with same co-ords, e.g. n53k7, 12-25s
          heta = 10 // i.e. 1 / (0.1 * 1), i.e. distance = 0.1*1, 1= min possible distance for integer coords
        } else {
          heta = 1 / distMatrixArray(loc)(n)
        }
        hetaList = heta :: hetaList
      }
      hetaList = hetaList.reverse
      hetaMatrix = hetaList :: hetaMatrix
    }
    hetaMatrix = hetaMatrix.reverse

    for (iteration <- 0 to (iterations - 1)) {

      System.out.println("#" + iteration + " iteration")

      for (group <- 0 to groups - 1) {

        //        var threadArray: Array[AntThread] = new Array(groups)
        //        for (i <- 0 to groups - 1) {
        //          threadArray(i) = new AntThread()
        //        }

        var currentGroupSize = groupSize
        if (group == groups - 1) {
          currentGroupSize = currentGroupSize + remainder
        }
        System.out.println("")

        var threadArray: Array[AntThread] = new Array(currentGroupSize) //NEW
        for (i <- 0 to currentGroupSize - 1) {
          threadArray(i) = new AntThread()
        }

        for (j <- 0 to (currentGroupSize - 1)) {
          val k = ants(j + group * groupSize)
          System.err.println("ant # " + (j + group * groupSize)) // index +group*(ants per group)
          threadArray(j).setParams(k, hetaMatrix, pheroMatrix)
          threadArray(j).start() //does all the work in ant
        } //end of ant loop
        threadArray.foreach(thr => thr.join())
      }

      // Choose best solution out of m ants

      var tourCostList: List[Double] = Nil
      //var tourCost: Double = 0.0
      ants.foreach { a =>
        a.tourCost = 0.0
        for (i <- 0 to (a.visitedNodesVector.size - 2)) {
          a.tourCost = distMatrixArray(a.visitedNodesVector.get(i))(a.visitedNodesVector.get(i + 1)) + a.tourCost
        }
        System.out.println("tourcost for ant " + a + " = " + a.tourCost + ", while solutionDistance = " + a.solutionDistance)
        //        System.err.println("a.visitedNodes = " + a.visitedNodes)
        tourCostList = a.tourCost :: tourCostList //for all ants
      }
      tourCostList = tourCostList.reverse

      System.out.println("")
      System.out.println("tourCostList: " + tourCostList)

      var bestSolutionAnt: Ant = null
      var bestSolutionCost = tourCostList.min
      var L = bestSolutionCost
      ants.foreach { a =>
        if (a.tourCost == bestSolutionCost)
          bestSolutionAnt = a
      }
      assert(bestSolutionAnt != null)

      var bestSolutionPath = bestSolutionAnt.visitedNodesVector

      //      System.out.println("bestSolutionPath: " + bestSolutionPath)
      //      System.out.println("bestSolutionAnt.routeMatrix: " + bestSolutionAnt.routeMatrix)
      //denorm:
      System.out.println("")
      System.out.print("bestSolutionPath: ")
      for (n <- 0 to bestSolutionPath.size - 1) {
        //      bestSolutionPath.foreach { n =>
        System.out.print((n + 1) + " ")
      }
      System.out.println("")
      System.out.print("bestSolutionAnt.routeMatrix: ")
      bestSolutionAnt.routeMatrix.foreach { l =>
        l.foreach { n =>
          System.out.print((n + 1) + " ")
        }
        System.out.print(", ")
      }
      System.out.println("")

      // global trail update for this solution - eqn (4)
      // increase pheromone on all arcs in best route.

      for (i <- 0 to (bestSolutionPath.size - 2)) {
        //        pheroMatrix = pheroMatrix.updated(bestSolutionPath(i), pheroMatrix(bestSolutionPath(i)).updated(bestSolutionPath(i + 1), (1 - alpha) * { pheroMatrix(bestSolutionPath(i))(bestSolutionPath(i + 1)) } + alpha * (1 / L)))
        val newPhero = ((1 - alpha) * { pheroMatrix(bestSolutionPath.get(i))(bestSolutionPath.get(i + 1)) } + alpha * (1 / L))
        pheroMatrix(bestSolutionPath.get(i)).update(bestSolutionPath.get(i + 1), newPhero)
      }

      iterSolutionSet.add(bestSolutionCost)
      System.out.println("bestSolutionCost: " + bestSolutionCost)
      System.out.println("iterSolutionList: " + iterSolutionSet)
      System.out.println("")

      //NEW: print the new pheromone values for the best solution, after being updated by eqn. 4:
      System.out.println("Pheromone values for the best solution, after being updated by eqn. 4:")
      bestSolutionAnt.routeMatrix.foreach { routeNodes =>
        for (i <- 0 to routeNodes.length - 1) { //CHECK this "length-2" everywhere, does it calculate ALL distances, even for last node in list?
          if (i == 0) {
            var pheroToDepot = pheroMatrix(depotsNorm.head)(routeNodes(i))
            //              System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
            //denorm:
            System.out.print(pheroToDepot + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")

          } else if (i == (routeNodes.length - 1)) {
            var phero = pheroMatrix(routeNodes(i - 1))(routeNodes(i))
            var pheroToDepot = pheroMatrix(routeNodes(i))(depotsNorm.head)
            //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")
            //denorm:
            System.out.print(" - " + phero + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + pheroToDepot + "p")

          } else {
            var phero = pheroMatrix(routeNodes(i - 1))(routeNodes(i))
            //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
            //denorm:
            System.out.print(" - " + "d, " + phero + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
          }
        }
      }

      System.out.println("")

      //NEW CODE: -- is this correct/ok? improvements between iterations now smaller (?)
      if (iteration == 4 | iteration == 9 | iteration == 29 | iteration == 49 | iteration == 99 | iteration == 199 | iteration == 299 | iteration == 399 | iteration == 499) {
        var iterBestSol = iterSolutionSet.min
        System.out.println("At iteration #" + iteration + ": ")
        System.out.println("best solution: " + iterBestSol)
        System.out.println("Known optimal solution: " + VRP.solCost)
        var percentOff = (iterBestSol - VRP.solCost) / VRP.solCost
        System.out.println("Off from optimal by: " + percentOff * 100 + " %.")
        var timeAtEnd = System.currentTimeMillis() / 1000 //seconds
        var duration = timeAtEnd - timeAtStart
        System.out.println("Time at Start: " + timeAtStart + " s, " + " Time at End: " + timeAtEnd + " s.")
        System.out.println("Time taken this far: " + duration + " s.")
      }

    } //end of iteration

    //choose best solution out of all iterations
    var iterBestSol = iterSolutionSet.min
    System.out.println("best solution: " + iterBestSol)
    System.out.println("Known optimal solution: " + VRP.solCost)
    var percentOff = (iterBestSol - VRP.solCost) / VRP.solCost
    System.out.println("Off from optimal by: " + percentOff * 100 + " %.")
    var timeAtEnd = System.currentTimeMillis() / 1000 //seconds
    var duration = timeAtEnd - timeAtStart
    System.out.println("Time at Start: " + timeAtStart + " s, " + " Time at End: " + timeAtEnd + " s.")
    System.out.println("Time taken by ACOalg: " + duration + " s.")
    System.out.println("This is 1-Colony NON-Synced PRL (w/ Mutable pheroMatrix).")
  }

  def twoOpt(routeNodes: List[Int]): List[Int] = {

    var result = routeNodes
    var routeNodesArray = Array.ofDim[Int](routeNodes.length)
    routeNodes.copyToArray(routeNodesArray)

    var swappedRouteNodes = false

    var routeNodesCost = 0.0
    for (j <- 0 to (routeNodesArray.length - 2)) {
      routeNodesCost = distMatrixArray(routeNodesArray(j))(routeNodesArray(j + 1)) + routeNodesCost
    }
    routeNodesCost += distMatrixArray(depot)(routeNodes(0))
    routeNodesCost += distMatrixArray(routeNodes(routeNodes.length - 1))(depot)

    var resCostArray = Array.ofDim[Double](routeNodes.length)
    var resultArray = Array.ofDim[List[Int]](routeNodes.length)

    for (i <- 0 to routeNodesArray.length - 2) {
      val t = routeNodesArray(i)
      //      var newRouteNodes = routeNodesArray
      var newRouteNodes = Array.ofDim[Int](routeNodes.length)
      routeNodes.copyToArray(newRouteNodes) //copy!, anti-aliasing

      newRouteNodes.update(i, routeNodesArray(i + 1))
      newRouteNodes.update(i + 1, t)

      // calculate costs -- make following into a function:
      var newRouteNodesCost = 0.0
      newRouteNodesCost += distMatrixArray(depot)(newRouteNodes(0))
      for (j <- 0 to (newRouteNodes.length - 2)) {
        newRouteNodesCost = distMatrixArray(newRouteNodes(j))(newRouteNodes(j + 1)) + newRouteNodesCost
      }
      newRouteNodesCost += distMatrixArray(newRouteNodes(newRouteNodes.length - 1))(depot)

      //compare costs
      if (newRouteNodesCost < routeNodesCost) {
        swappedRouteNodes = true
        result = newRouteNodes.toList
        resultArray.update(i, result)
        resCostArray.update(i, newRouteNodesCost)
      }
    }

    //do 2-opt swapping last and 1st:
    val t = routeNodesArray(routeNodesArray.length - 1)
    //      var newRouteNodes = routeNodesArray
    var newRouteNodes = Array.ofDim[Int](routeNodes.length)
    routeNodes.copyToArray(newRouteNodes) //copy!, anti-aliasing
    newRouteNodes.update(routeNodesArray.length - 1, routeNodesArray(0))
    newRouteNodes.update(0, t)

    //calculate costs:
    var newRouteNodesCost = 0.0
    newRouteNodesCost += distMatrixArray(depot)(newRouteNodes(0)) //!
    for (j <- 0 to (newRouteNodes.length - 2)) {
      newRouteNodesCost = distMatrixArray(newRouteNodes(j))(newRouteNodes(j + 1)) + newRouteNodesCost
    }
    newRouteNodesCost += distMatrixArray(newRouteNodes(newRouteNodes.length - 1))(depot) //!

    //compare costs
    if (newRouteNodesCost < routeNodesCost) {
      swappedRouteNodes = true
      result = newRouteNodes.toList
      resultArray.update((resultArray.length) - 1, result)
      resCostArray.update((resCostArray.length) - 1, newRouteNodesCost)
    }
    //end of 2-opt swapping last and 1st.

    //    return result

    //    val acceptResCostArray = resCostArray.filterNot(n => n == 0.0)
    //    if (!acceptResCostArray.isEmpty) {
    if (swappedRouteNodes == true) {
      val acceptResCostArray = resCostArray.filterNot(n => n == 0.0)
      val minimum = acceptResCostArray.min
      val index = resCostArray.indexOf(minimum)
      return resultArray(index)
    } else return routeNodes
  } //end of 2opt
}